# current AWS user
data "aws_caller_identity" "current" {}

# current AWS region
data "aws_region" "current" { }

# All availability zones
data "aws_availability_zones" "all" { }

# Scotty's home IP
# Find Scotty's current home IPv4 address
# home.catbert.net is automatically updated if the Comcast IP changes
data "dns_a_record_set" "scotty" {
  host = "home.catbert.net"
}

# Latest Debian Buster AMI
data "aws_ami" "buster" {
  most_recent = true
  name_regex  = "debian-10-amd64*"
  owners      = ["136693071363"]
}

data "aws_iam_policy_document" "ec2_assume_role" {
  statement {

    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"
      identifiers = [ "ec2.amazonaws.com" ]
    }

  }
}

data "aws_iam_policy_document" "dns_update" {

  statement {
    actions   = [
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets"
    ]
    resources = [
      "arn:aws:route53:::hostedzone/${aws_route53_zone.terraformer.zone_id}"
    ]
  }

  statement {
    actions = [
      "ec2:DescribeTags",
      "route53:GetChange",
      "route53:ListHostedZones",
      "route53:ListHostedZonesByName"
    ]
    resources = [
      "*"
    ]
  }
}
