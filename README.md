# tf_aws_audit_terraformer

Sets up an EC2 instance in the audit account for running terraform

* Creates a new VPC in the audit account
  * Creates a security group to allow SSH access from a restricted set of CIDRs
* Creates a new Route53 Hosted Zone for _domain\_name_
* Creates an EFS filesystem to provide persistent storage
* Creates an IAM role (_terraformer_) and instance profile
* Creates an IAM policy (_dns\_update_) to allow updating _domain\_name_ in Route53
* Adds the _dns\_update_ and _AdministratorAccess_ policies to the role
* Creates a new autoscaling group and launch configuration to keep an EC2 instance running
  * Uses [Cloudinit](https://cloudinit.readthedocs.io/en/latest/topics/modules.html) (via the [Terraform](https://terraform.io) [cloudinit_config](https://registry.terraform.io/providers/hashicorp/cloudinit/latest/docs/data-sources/cloudinit_config) provider) to configure users, packages, etc. when instance is created

## Variables

| Name          | Type   | Description                    | Default                      |
|---------------|--------|--------------------------------|------------------------------|
| `domain_name` | string | domain / FQDN for EC2 instance | iso-terraformer.stanford.edu |
| `vpc_cidr`    | string | CIDR used for VPC              | 10.0.0.0/16                  |

## Usage

This module should only be instantiated once per account, and will run in the default region.

```terraform
module "terraformer" {
  source       = "git::https://code.stanford.edu/sc2_public/tf_aws_audit_terraformer.git"
  domain_name  = "terraformer.itlab.stanford.edu"
}
```

## Configuring the AWS CLI

Each user on the terraformer instance must configure the AWS CLI in their home directory.

### Production

* Create _~/.aws/config_ with this content:
```
[default]
output = json
region = us-west-2

[profile su-aws-audit]
credential_source = Ec2InstanceMetadata

[profile su-aws-main-terraform]
role_arn = arn:aws:iam::805715643564:role/terraform
credential_source = Ec2InstanceMetadata
```

### Testing

* Create _~/.aws/config_ with this content:
```
[default]
output = json
region = us-west-2

[profile uit-aws-audit]
credential_source = Ec2InstanceMetadata

[profile uit-itlab]
role_arn = arn:aws:iam::077108534746:role/terraform
credential_source = Ec2InstanceMetadata
```

## NOTES

1. The Route53 hosted zone needs delegation records in an upstream domain; this is not handled by this module
2. Add / remove users or their authorized SSH keys by modifing _userdata/users-groups.yaml_

## Related Modules

* [AWS Audit](https://code.stanford.edu/sc2_public/tf_aws_audit/)
