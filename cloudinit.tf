locals {
  install_sh = filebase64("${path.module}/userdata/install.sh")
  dns_update = filebase64("${path.module}/userdata/dns-update")
}

data "cloudinit_config" "terraformer" {
  gzip = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content      = "timezone: PST8PDT\nntp:\n  enabled: true"
  }

  part {
    content_type = "text/cloud-config"
    content      = file("${path.module}/userdata/packages.yaml")
  }

  part {
    content_type = "text/cloud-config"
    content      = "mounts:\n  - [ '${aws_efs_file_system.efs.dns_name}:/', '/efs', 'nfs4', 'rw,relatime,vers=4.1,rsize=1048576,wsize=1048576,hard,noresvport,proto=tcp,timeo=600,retrans=2,sec=sys' ]"
  }

  # apparently, UIDs have to be strings otherwise they are ignored
  part {
    content_type = "text/cloud-config"
    content      = file("${path.module}/userdata/users-groups.yaml")
  }

  part {
    content_type = "text/cloud-config"
    content      = <<WRITE_FILES
write_files:
- encoding: b64
  owner: root:root
  permissions: '0755'
  path: /tmp/install.sh
  content: ${local.install_sh}
- encoding: b64
  owner: root:root
  permissions: '0755'
  path: /etc/network/if-up.d/dns-update
  content: ${local.dns_update}
WRITE_FILES
  }

  part {
    content_type = "text/cloud-config"
    content      = <<RUNCMDS
runcmd:
 - /tmp/install.sh
 - /etc/network/if-up.d/dns-update
 - /bin/mkdir -p /efs/aws
 - /bin/chgrp terraform /efs/aws
 - /bin/chmod g+rwsx /efs/aws
 - /bin/ln -s -f /efs/aws/tools/import-aws /usr/local/bin/import-aws
 - /bin/pip3 install -U awscli
RUNCMDS
  }
  
}

