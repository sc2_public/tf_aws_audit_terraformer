# ----- TERRAFORMER -----

resource "aws_security_group" "terraformer" {
  name        = "terraformer"
  vpc_id      = aws_vpc.terraformer.id
  description = "terraformer SG"

  egress {
    description      = "Allow all outbound traffic"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = local.cidr_v4_anywhere
    ipv6_cidr_blocks = local.cidr_v6_anywhere
  }

  ingress {
    description      = "SSH from campus and homes"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = local.cidr_v4_homes_su
    ipv6_cidr_blocks = local.cidr_v6_homes_su
  }

  ingress {
    description = "Allow all ICMP"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = local.cidr_v4_anywhere
  }

  ingress {
    description      = "Allow all ICMPv6"
    from_port        = -1
    to_port          = -1
    protocol         = "icmpv6"
    ipv6_cidr_blocks = local.cidr_v6_anywhere
  }

  ingress {
    description      = "Allow access to EFS"
    from_port        = 2049
    to_port          = 2049
    protocol         = "tcp"
    cidr_blocks      = [ aws_vpc.terraformer.cidr_block ]
    ipv6_cidr_blocks = [ aws_vpc.terraformer.ipv6_cidr_block ]
  }

  tags = {
    Name = "terraformer"
  }
}
