variable "domain_name" {
  description = "FQDN for terraformer host"
  type        = string
  default     = "iso-terraformer.stanford.edu"
}

variable "vpc_cidr" {
  description = "internal VPC IPv4 CIDR"
  type        = string
  default     = "10.0.0.0/16"
}

