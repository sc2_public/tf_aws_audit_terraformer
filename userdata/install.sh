#! /bin/bash

# get Hashicorp GPG key
gpg --no-tty --keyserver keyserver.ubuntu.com --recv-keys 51852D87348FFC4C

mkdir -p /usr/local/bin /tmp/hashicorp
cd /tmp/hashicorp

for PKG in packer terraform; do
  V=$(git ls-remote --tags git://github.com/hashicorp/${PKG} |\
        egrep '^.*/tags/v[0-9]+\.[0-9]+\.[0-9]+$' |\
        sed 's,^.*/tags/v,,' |\
        sort -g -t. -k 1,1n -k 2,2n -k 3,3n |\
        tail -1)
  P=${PKG}_${V}
  ZIP=${P}_linux_amd64.zip
  SHA=${P}_SHA256SUMS
  SIG=${SHA}.sig
  BIN=/usr/local/bin/${PKG}

  curl -Os https://releases.hashicorp.com/${PKG}/${V}/${ZIP}
  curl -Os https://releases.hashicorp.com/${PKG}/${V}/${SHA}
  curl -Os https://releases.hashicorp.com/${PKG}/${V}/${SIG}

  if gpg --no-tty --verify ${SIG} ${SHA} 2>/dev/null && grep ${ZIP} ${SHA} | shasum -a 256 -c -
  then
    unzip ${ZIP}
    mv ${PKG} ${BIN}
    chmod +x ${BIN}
  fi
done

cd /
rm -rf /tmp/hashicorp

