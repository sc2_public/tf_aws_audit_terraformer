resource "aws_iam_instance_profile" "terraformer" {
  name = "terraformer"
  role = aws_iam_role.terraformer.name
}

resource "aws_iam_role" "terraformer" {
  name               = "terraformer"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
}

resource "aws_iam_role_policy_attachment" "terraformer_dns" {
  role       = aws_iam_role.terraformer.name
  policy_arn = aws_iam_policy.dns_update.arn
}

resource "aws_iam_role_policy_attachment" "terraformer_admin" {
  role       = aws_iam_role.terraformer.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_policy" "dns_update" {
  name   = "dns_update"
  policy = data.aws_iam_policy_document.dns_update.json
}
