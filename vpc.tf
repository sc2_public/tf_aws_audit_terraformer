# Terraformer VPC configuration

resource "aws_vpc" "terraformer" {
  cidr_block                       = var.vpc_cidr
  enable_dns_support               = true
  enable_dns_hostnames             = true
  assign_generated_ipv6_cidr_block = true

  tags = {
    Name = "terraformer"
  }
}

resource "aws_internet_gateway" "terraformer" {
  vpc_id = aws_vpc.terraformer.id
}

resource "aws_route_table" "terraformer" {
  vpc_id = aws_vpc.terraformer.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terraformer.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.terraformer.id
  }
}

resource "aws_subnet" "terraformer" {
  count                           = local.az_count
  vpc_id                          = aws_vpc.terraformer.id
  availability_zone               = data.aws_availability_zones.all.names[count.index]
  cidr_block                      = cidrsubnet(aws_vpc.terraformer.cidr_block,      10, count.index)
  ipv6_cidr_block                 = cidrsubnet(aws_vpc.terraformer.ipv6_cidr_block,  8, count.index)
  map_public_ip_on_launch         = true
  assign_ipv6_address_on_creation = true

  tags = {
    Name    = "terraformer-${substr(data.aws_availability_zones.all.names[count.index],9,1)}"
    Subnet  = "terraformer"
  }
}

resource "aws_route_table_association" "terraformer" {
  count          = local.az_count
  route_table_id = aws_route_table.terraformer.id
  subnet_id      = aws_subnet.terraformer.*.id[count.index]
}

