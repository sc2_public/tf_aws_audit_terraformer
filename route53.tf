# Route53 reusable delegation set
resource "aws_route53_delegation_set" "terraformer" {
}

resource "aws_route53_zone" "terraformer" {
  name              = var.domain_name
  comment           = "Primary domain"
  delegation_set_id = aws_route53_delegation_set.terraformer.id

  tags = {
    Name = var.domain_name
  }
}

resource "aws_route53_record" "terraformer" {
  zone_id = aws_route53_zone.terraformer.zone_id
  name    = var.domain_name
  type    = "A"
  ttl     = 60
  records = ["10.0.0.1"]
  lifecycle {
    ignore_changes = [records]
  }
}

resource "aws_route53_record" "terraformer_ipv6" {
  zone_id = aws_route53_zone.terraformer.zone_id
  name    = var.domain_name
  type    = "AAAA"
  ttl     = 60
  records = ["::1"]
  lifecycle {
    ignore_changes = [records]
  }
}

