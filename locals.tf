locals {
  aws_account_id    = data.aws_caller_identity.current.account_id
  aws_region        = data.aws_region.current.name
  aws_instance_type = "t2.medium"

  az_count          = length(data.aws_availability_zones.all.names)

  cidr_v4_anywhere  = [ "0.0.0.0/0" ]
  cidr_v6_anywhere  = [ "::/0" ]
  cidr_v4_su        = [ "171.64.0.0/14" ]
  cidr_v6_su        = [ "2607:f6d0::/32" ]
  cidr_v4_su_vpn    = [ "171.66.0.0/16" ]
  cidr_v4_homes     = [
    # Scotty
    formatlist("%s/32", data.dns_a_record_set.scotty.addrs),
  ]
  cidr_v6_homes     = [
    # part of Scotty's persistent IPv6 /60 from Comcast
    "2601:646:c500:caa2::/64",
  ]
}

locals {
  iam_base_uri     = "arn:aws:iam::${local.aws_account_id}"
  s3_backup_name   = "${local.aws_account_id}-backup"
  s3_backup_arn    = "arn:aws:s3:::${local.s3_backup_name}"

  cidr_v4_homes_su = flatten([ local.cidr_v4_su, local.cidr_v4_homes ])
  cidr_v6_homes_su = flatten([ local.cidr_v6_su, local.cidr_v6_homes ])
}
