# Terraformer persistent storage
resource "aws_efs_file_system" "efs" {
  tags = {
    Name    = "terraformer-efs"
    backup  = "daily"
  }
}

resource "aws_efs_mount_target" "efs" {
  count           = local.az_count
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = aws_subnet.terraformer[count.index].id
  security_groups = [
    aws_security_group.terraformer.id
  ]
}
