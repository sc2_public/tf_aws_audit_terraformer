#
# terraformer autoscale group configuration
#

resource "aws_autoscaling_group" "terraformer" {
  name                 = "terraformer"
  min_size             = 1
  max_size             = 1
  desired_capacity     = 1

  health_check_type    = "EC2"
  force_delete         = true

  launch_configuration = aws_launch_configuration.terraformer.name
  vpc_zone_identifier  = aws_subnet.terraformer.*.id

  # Name
  tag {
    key                 = "Name"
    value               = "terraformer"
    propagate_at_launch = true
  }

  # hostname
  tag {
    key                 = "hostname"
    value               = var.domain_name
    propagate_at_launch = true
  }

  # zone
  tag {
    key                 = "zone"
    value               = aws_route53_zone.terraformer.zone_id
    propagate_at_launch = true
  }

  # backup
  tag {
    key                 = "backup"
    value               = "daily"
    propagate_at_launch = true
  }

}

resource "aws_launch_configuration" "terraformer" {
  name_prefix          = "terraformer-"
  image_id             = data.aws_ami.buster.id
  instance_type        = "t3.small"
  iam_instance_profile = aws_iam_instance_profile.terraformer.name

  security_groups      = [
    aws_security_group.terraformer.id,
  ]
  lifecycle {
    create_before_destroy = true
  }
  user_data = data.cloudinit_config.terraformer.rendered
}
